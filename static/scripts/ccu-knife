#!/bin/sh
# SPDX-License-Identifier: MIT
# Author: Sjoerd Simons <sjoerd@collabora.com>
#
# Wrapper for knife to avoid the client cert to ever be stored unencrypted on
# disk. This wrapper requires unpriviledged user namespaces to be enabled and
# the client certificate sorted by `pass` (https://www.passwordstore.org/)
#
# The way it works is that a new user namespace is created with most
# among other things a sepearte mount namespace; In that namespace a tmpfs is
# mounted on ~/.cinc and configuration is setup with the client cert pulled out
# of pass.
#
# This all means the unencrypted certificate is only ever in a) memory
# (tmpfs) and b) visible only for programs in the same mount namespace until
# the mount namespaces gets shut down; So for an attacker to get the client key
# it there is only a quite short time-window and tbh. attacking pass when the
# gnupg agent is unlocked is simpler :).  Practically the real benefit of this
# approach is avoiding the wrapper is accidentally leaving the client pem file
# accessible for a long time due to cleanup failing

set -e

# Client name for chef
USER=sjoerdR
# Chef server
SERVER=https://chef.collabora.co.uk
# Name of the client key in pass
PASSCERT=collabora/chef/client.pem

if [ $$ -ne 1 ] ; then
 exec unshare --kill-child \
              --fork \
              --pid  \
              --mount-proc \
              --map-root-user \
              --mount \
              $0 $@
 # Will only get here if exec fails
 exit 1
fi

CONFIGDIR=${HOME}/.cinc
mkdir -p ${CONFIGDIR}
# Support for older non-cinc tooling
ln -Tsf ${CONFIGDIR} ${HOME}/.chef

mount -t tmpfs none ${CONFIGDIR}

cat << EOF > ${CONFIGDIR}/knife.rb
node_name                '${USER}'
client_key               '${CONFIGDIR}/client.pem'
chef_server_url          '${SERVER}'
EOF

pass ${PASSCERT} > ${CONFIGDIR}/client.pem

exec knife $@
